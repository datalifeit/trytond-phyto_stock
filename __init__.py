# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .stock import StockConfiguration, StockShipmentOut, StockMove
from .phyto import MoveReport, MoveReportPrint, MoveReportPrintStart


def register():
    Pool.register(
        StockConfiguration,
        StockShipmentOut,
        StockMove,
        MoveReportPrintStart,
        module='phyto_stock', type_='model')
    Pool.register(
        MoveReportPrint,
        module='phyto_stock', type_='wizard')
    Pool.register(
        MoveReport,
        module='phyto_stock', type_='report')
