# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.model import Workflow
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id, Not, In
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning

_STATES = {'readonly': Not(In(Eval('state'), ['draft', 'waiting']))}


class StockConfiguration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    phyto_party_required = fields.Boolean('Phyto Party Required')


class StockShipmentOut(metaclass=PoolMeta):
    _error_messages = None
    __name__ = 'stock.shipment.out'

    phyto_party = fields.Many2One('party.party', 'Phyto Party',
        domain=[('categories', 'child_of',
            Id('phyto', 'phyto_party_applier'), 'parent'),
            ('phyto_party', '=', Eval('customer'))],
        depends=['customer', 'state'], states=_STATES)

    @classmethod
    @Workflow.transition('assigned')
    def assign(cls, shipments):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        Config = pool.get('stock.configuration')
        config = Config(1)
        phyto_categories = cls.get_phyto_categories()

        for shipment in shipments:
            if any(c in phyto_categories for move in shipment.moves
                    for c in move.product.categories):
                if shipment.phyto_party:
                    continue
                if config.phyto_party_required:
                    raise UserError(gettext(
                        'phyto_stock.'
                        'msg_stock_shipment_out_phyto_party_recommended',
                        shipment=shipment.id))
                else:
                    warning_name = ('phyto_party_recommended_%s'
                        % shipment.customer.id)
                    if Warning.check(warning_name):
                        raise UserWarning(warning_name, gettext(
                            'phyto_stock.'
                            'msg_stock_shipment_out_phyto_party_recommended',
                            shipment=shipment.id))

        super().assign(shipments)

    @classmethod
    def get_phyto_categories(cls):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        _Category = pool.get('product.category')

        _id = Modeldata.get_id('phyto', 'phyto_product_phytosanitary')

        cats = _Category.search([('parent', 'child_of', _id)])
        return cats


class StockMove(metaclass=PoolMeta):
    __name__ = 'stock.move'

    party = fields.Function(fields.Many2One('party.party', 'Party'),
        'get_party')
    shipment_type = fields.Function(fields.Selection(
        [('in', 'In'), ('out', 'Out')], 'Shipment Type'), 'get_shipment_type')
    phyto_number = fields.Function(fields.Char('Phytosanitary Number'),
        'get_phyto_number')
    phyto_party = fields.Function(fields.Many2One('party.party',
        'Phyto Party'), 'get_phytosanitary_party')

    def get_party(self, name):
        if self.shipment:
            if hasattr(self.shipment, 'customer'):
                return self.shipment.customer.id
            elif hasattr(self.shipment, 'supplier'):
                return self.shipment.supplier.id
        return None

    @classmethod
    def get_shipment_type(cls, moves, name):
        res = {move.id: '' for move in moves}
        values = {
            'stock.shipment.in': 'in',
            'stock.shipment.in.return': 'out',
            'stock.shipment.out': 'out',
            'stock.shipment.out.return': 'in',
        }
        for move in moves:
            if move.shipment:
                res[move.id] = values[move.shipment.__name__]
        return res

    @classmethod
    def get_phytosanitary_party(cls, moves, name):
        res = {move.id: None for move in moves}
        for move in moves:
            if move.shipment:
                if getattr(move.shipment, 'phyto_party', None):
                    res[move.id] = move.shipment.phyto_party.id
        return res

    def get_phyto_number(self, name):
        if self.product:
            return self.product.phyto_number
        return None
