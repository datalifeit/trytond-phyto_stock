====================
Phyto Stock Scenario
====================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> import datetime
    >>> from proteus import Model, Wizard
    >>> from decimal import Decimal
    >>> today = datetime.date.today()
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company

Install phyto_stock::

    >>> config = activate_modules('phyto_stock')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company

Create Party Category::

    >>> PartyCategory = Model.get('party.category')
    >>> party_category, = PartyCategory.find([('name', '=', 'Cualified')])
    >>> party_category.save()

Create Phyto Staff::

    >>> phyto_staff = Party(name='Phyto Staff')
    >>> phyto_staff.categories.append(party_category)
    >>> phyto_staff.phyto_party.append(customer)
    >>> phyto_staff.save()

Create Stock Config::

    >>> StockConfig = Model.get('stock.configuration')
    >>> configuration = StockConfig()
    >>> configuration.save()

Add a shipment line::

    >>> StockMove = Model.get('stock.move')
    >>> move = StockMove()
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 1
    >>> move.from_location = output_loc
    >>> move.to_location = customer_loc
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> move.save()
    >>> shipment_out.outgoing_moves.append(move)
    >>> shipment_out.save()

Make 1 unit of the product available::

    >>> incoming_move = StockMove()
    >>> incoming_move.product = product
    >>> incoming_move.uom = unit
    >>> incoming_move.quantity = 1
    >>> incoming_move.from_location = supplier_loc
    >>> incoming_move.to_location = storage_loc
    >>> incoming_move.planned_date = today
    >>> incoming_move.effective_date = today
    >>> incoming_move.company = company
    >>> incoming_move.unit_price = Decimal('1')
    >>> incoming_move.currency = company.currency
    >>> incoming_move.click('do')

Set the shipment state to waiting::

    >>> shipment_out.click('wait')

When Category is not Phytosanitary::

    >>> shipment_out.click('assign_try')
    True

When Phyto Party is required::

    >>> shipment_out.click('wait')
    >>> category, = ProductCategory.find([('name', '=', 'Phytosanitary')])
    >>> template.categories.append(category)
    >>> template.save()
    >>> product.template = template
    >>> product.save()
    >>> configuration.phyto_party_required = True
    >>> configuration.save()
    >>> shipment_out.click('assign_try') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Shipment ... contains phytosanitary products but there is no phytosanitary party assigned. - 

When Phyto Party is no required::

    >>> configuration.phyto_party_required = False
    >>> configuration.save()
    >>> shipment_out.click('assign_try')# doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserWarning: Shipment ... contains phytosanitary products but there is no phytosanitary party assigned. - 

When there is a Phyto Party::

    >>> shipment_out.phyto_party = phyto_staff
    >>> shipment_out.click('assign_try')
    True

Launch Phyto Party Report::

    >>> shipment_out.click('done')
    >>> move_report = Wizard('phyto.move_report.print')
    >>> move_report.form.initial_date = today
    >>> move_report.form.end_date = today
    >>> move_report.execute('print_')

